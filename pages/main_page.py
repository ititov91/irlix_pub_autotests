from selenium.webdriver.common.by import By
from pages.base_page import BasePage
from resources import drinkName, searchInput


class MainPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    search_button = (By.CLASS_NAME, "search__button")
    search_bar = (By.XPATH, "//*/input")
    drink_item = (By.CLASS_NAME, "drinks-item") #элемент с коктейлем
    drink = (By.XPATH, f"//*/h2[normalize-space(text())='{drinkName}']") #коктейль по имени из Resources
    main_title = (By.XPATH, "//*/h1[normalize-space(text())='Главная']")

    def count_drinks(self):
        return self.count_elements(self.drink_item)

    def search(self):
        self.click(self.search_button)
        self.send_keys(self.search_bar, searchInput)

    # перейти к коктейлю по его имени
    def go_to_drink(self):
        self.click(self.drink)

    # получить h1 заголовок страницы
    def get_page_title(self):
        return self.get_element_text(self.main_title)











