from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage():

    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 7)

    def click(self, by_locator):
        self.wait.until(EC.presence_of_element_located(by_locator)).click()

    def wait_for(self, by_locator):
        self.wait.until(EC.presence_of_element_located(by_locator))

    def send_keys(self, by_locator, value):
        self.wait.until(EC.presence_of_element_located(by_locator)).send_keys(value)

    def get_element(self, by_locator):
        return self.wait.until(EC.presence_of_element_located(by_locator))

    def count_elements(self, by_locator):
        return len(self.wait.until(EC.presence_of_all_elements_located(by_locator)))

    def get_element_text(self, by_locator):
        return self.wait.until(EC.presence_of_element_located(by_locator)).get_attribute("innerText")

    def get_all_elements(self, by_locator):
        self.wait.until(EC.presence_of_all_elements_located(by_locator))
