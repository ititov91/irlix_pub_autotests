from selenium.webdriver.common.by import By
from pages.base_page import BasePage
from resources import searchInput


class CocktailPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    cocktail_title = (By.XPATH, "//*/h1")
    back_button = (By.CLASS_NAME, "back-button__arrow")

    def get_cocktail_title(self):
        return self.get_element_text(self.cocktail_title)

    def click_back_button(self):
        self.click(self.back_button)





