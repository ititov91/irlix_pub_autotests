import resources
from tests.test_base import BaseTest
import pytest_check as check


class TestCompareTitles(BaseTest):

    def test_compare_titles(self):
        self.MainPage.go_to_drink()
        check.equal(self.CocktailPage.get_cocktail_title(), resources.drinkName)

