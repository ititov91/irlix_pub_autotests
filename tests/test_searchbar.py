import time
import resources
from tests.test_base import BaseTest
import pytest_check as check


class TestSearch(BaseTest):

    #Проверяем работу поиска, сравнение количества ожидаемых элементов и проверка содержания заголовка (WIP)

    def test_search(self):
        time.sleep(4)
        self.MainPage.search()
        check.equal(self.MainPage.count_drinks(), resources.searchResult)

