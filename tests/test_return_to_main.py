import resources
from tests.test_base import BaseTest
import pytest_check as check
import time


class TestReturnToMain(BaseTest):

    # Проверяем работу кнопки "назад" в карточке коктейля

    def test_return_to_main(self):
        self.MainPage.go_to_drink()
        self.CocktailPage.click_back_button()
        time.sleep(5) #Я в курсе что плохо, как починить пока еще не придумали! EC.waitfor не работает с этой кнопкой и кнопкой поиска
        check.equal(resources.MainPageH1, self.MainPage.get_page_title())
