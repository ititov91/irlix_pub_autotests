import resources
from tests.test_base import BaseTest
import pytest_check as check


class TestCount(BaseTest):

    # проверяем количество элементов с напитками на главной странице

    def test_count(self):
        check.equal(self.MainPage.count_drinks(), resources.drinksNumber)
