from selenium import webdriver
import pytest
import resources
from pages.main_page import MainPage
from pages.cocktail_page import CocktailPage


@pytest.fixture
def getDriver(request):
    global driver
    driver = webdriver.Chrome(resources.PATH)
    driver.get(resources.url)
    request.cls.MainPage = MainPage(driver)
    request.cls.CocktailPage = CocktailPage(driver)
    yield driver
    driver.quit()








